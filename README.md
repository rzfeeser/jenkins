# Jenkins

## Introduction

This repository has a collection of Jenkins tools:
- Jenkins Workers
    - ssh-ansibleansible
    - ssh-ansiblepython
    - ssh-ansibleterraform
    - docker-pythonworker


## .gitlab-ci.yml (Automation)

The `.gitlab-ci.yml` file within this repository will cause all Dockerfiles to be run, and the respective images to be pushed to this container image repository.


## Building Jenkins Workers (Manually)

This repository contains a `.gitlab-ci.yml` which (re)builds all images anytime an edit is made to the main branch. However, to build an image manually:

1. **Clone the repository**

    `git clone https://github.com/rzfeeser/jenkins/`

0. **Build your agent.** Each Agent has its own Dockerfile

    ```
    # Example: Building Terraform SSH Agent
    sudo docker build -t registry.gitlab.com/rzfeeser/jenkins/ssh-agentterraform:latest .
    ```


## Pushing Docker images to GitLab Container Registry (Manually)

1. **Generate an access token.** - Within GitLab, generate an access token to use in lieu of your password.

0. **Log into registry.gitlab.com** - Edit the following by replacing the `<expressions>` with your custom values

    `docker login -u` <gitlab username> `-p` <access token> `registry.gitlab.com`

0. **Push the built images to GitLab** - Run the following command to push the image to GitLab's registry.

    `sudo docker push registry.gitlab.com/rzfeeser/jenkins/ssh-agentterraform:latest`



## Resources
- [GitLab - Pipelines](https://docs.gitlab.com/ee/ci/pipelines/)
- [GitLab - .gitlab-ci.yml](https://docs.gitlab.com/ee/ci/yaml/gitlab_ci_yaml.html)
- [GitLab - .gitlab-ci.yml Keyword Reference](https://docs.gitlab.com/ee/ci/yaml/)


## Author
@RZFeeser - Author & Instructor - Feel free to reach out to if you're looking for a instructor led training solution.
